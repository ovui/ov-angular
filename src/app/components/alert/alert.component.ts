import { Component, OnInit, Input } from '@angular/core';
import { AlertService } from './alert.service';
import { filter } from 'rxjs/operators'; 

@Component({
  selector: 'alert-component',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @Input() id: string;
  alertInstances: any = [];
  alertObject: any;

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    this.alertService.getAlertObject()
      .pipe(filter((alert: any) => alert.id === this.id))
      .subscribe((alertObject: any) => {
        this.alertInstances.push(alertObject);
    });
  }

  removeAlert(alert: any) {
    this.alertInstances = this.alertInstances.filter((item: any) => item !== alert);  
  }
}
