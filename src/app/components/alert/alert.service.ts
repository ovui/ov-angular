import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class AlertService {
  subject = new Subject<any>();
  constructor() { }

  success(msg: string, id: string) {
    this.subject.next({id: id, type: 'success', message: msg });
  }

  error(msg: string, id: string, ) {
    this.subject.next({id: id,  type: 'danger', message: msg });
  }

  getAlertObject(): Observable<any> {
    return this.subject.asObservable();
  }
}
