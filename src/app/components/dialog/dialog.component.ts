import { Component, OnInit, Input, Inject } from '@angular/core';
import { DialogService } from './dialog.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'dialog-component',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  @Input() id: string;
  dialogObject: any = {};
  isShowDlg: any = false;
  backdrop: HTMLElement

  constructor(
    private dialogService: DialogService,
    @Inject(DOCUMENT) private document: Document,
   ) { }

  ngOnInit() {
    this.dialogService.getDialogObject()
      .subscribe((dlgObject: any) => {
        if(this.isShowDlg && dlgObject) {
          return;
        }
        if (!dlgObject) {
          this.closeDialog();
        } else {
          this.isShowDlg = true;
          this.dialogObject = dlgObject;
          this.showBackdrop();
        }
    });
  }

  closeDialog() {
    this.isShowDlg = false;  
    this.dialogObject = {};
    this.hideBackdrop();
  }

  showBackdrop() {
    this.backdrop = this.document.createElement('div');
    this.backdrop.classList.add('modal-backdrop');
    this.backdrop.classList.add('show');
    this.document.body.appendChild(this.backdrop);
  }

  hideBackdrop() {
    this.document.body.classList.remove('modal-open');
    this.backdrop.remove();
  }

}
