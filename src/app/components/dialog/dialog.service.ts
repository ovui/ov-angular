import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class DialogService {
  subject = new Subject<any>();
  constructor() { }

  open(dlgObject: any) {
    this.subject.next({
      id: dlgObject.id || 'dialog-id', 
      title: dlgObject.title || 'Dialog title',
      body: dlgObject.body || 'Dialog body',
      listAction: dlgObject.listAction || []
    });
  }

  close() {
    this.subject.next(false);
  }

  getDialogObject(): Observable<any> {
    return this.subject.asObservable();
  }
}
