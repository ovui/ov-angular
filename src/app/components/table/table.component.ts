  import { Component, Input, OnInit, Output, EventEmitter, TemplateRef, OnChanges, SimpleChanges } from '@angular/core';
  import { FullwidthDetailComponent } from './components/fullwidth-detail';

  @Component({
    selector: 'table-component',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
  })
  export class TableComponent implements OnInit, OnChanges {
    @Input() config: any = {};
    @Input() data: any = [];
    @Input() detailTemplate?: TemplateRef<any>;
    @Output() onSelectedListChanged: EventEmitter<any> = new EventEmitter();
    // @Output() onGridReadyCallback: EventEmitter<any> = new EventEmitter();

    gridApi: any;
    columnApi: any;
    defaultConfig: any = {};
    columnDefs: any = [];
    rowData: any = [];
    selectedList: any = [];
    searchValue: string = '';
    frameworkComponents: any;
    isFullWidthCell: any;
    isRowSelectable: any;
    fullWidthCellRenderer: any;
    getRowHeight: any;
    loadingTemplate: string = `<span class="ag-overlay-loading-center">Data is loading...</span>`;
    noRowsTemplate: string = `<span class="ag-overlay-loading-center">No rows to show</span>`;
    isShowDetail: boolean = false;
    isSort: boolean = false;

    constructor() {
      this.fullWidthCellRenderer = 'fullWidthCellRenderer'; 
      this.frameworkComponents = { fullWidthCellRenderer: FullwidthDetailComponent };
      this.isFullWidthCell = function(rowNode: any) {
        let isFullWidth = rowNode.data && rowNode.data.isFullWidthRow ? rowNode.data.isFullWidthRow : false;
        return isFullWidth;
      };
      this.isRowSelectable = function(rowNode: any) {
        return rowNode.data && !rowNode.data.isFullWidthRow ;
      };
      this.getRowHeight = function(params) {
        let isFullWidth = params.data && params.data.isFullWidthRow ? params.data.isFullWidthRow : false;
        if (isFullWidth) {
          return 120;
        } else {
          return 32;
        }
      };
    }

    onGridReady(params: any) {
      if(!this.config.columns) {
        console.log('Missing data or table config !');
        return;
      }
      this.gridApi = params.api;
      this.columnApi = params.columnApi;
      this.gridApi.showLoadingOverlay();
      window.onresize = () => {
        this.gridApi.sizeColumnsToFit();
      }
    }

    onSelectionChanged(params: any): void {
      this.selectedList = params.api.getSelectedRows();
      this.onSelectedListChanged.emit(this.selectedList);
      this.isShowDetail = this.selectedList.length === 1;
    }

    onRowClicked(params: any): void {
      if(this.config.showDetailRight || this.searchValue || this.isSort) {
        return;
      }
      params.node.isCustomDetail = !params.node.isCustomDetail;
      if(!params.data.isFullWidthRow){
        if(params.node.isCustomDetail) {
          this.onAddCustomRow(params);
        } else {
          this.onRemoveCustomRow(params);
        }
      }
    }

    onAddCustomRow(params: any) {
      let newItem = { isFullWidthRow: true, parentId: params.rowIndex }; // Add new row as a full width row. Using flag isFullWidthRow to detect.
      this.gridApi.updateRowData({
        add: [newItem],
        addIndex: params.rowIndex + 1
      });
      // Remove expanded row before adding new row.
      let nodes = params.api.getRenderedNodes();
      for (let index = 0; index < nodes.length; index++) {
        let node = nodes[index];
        if(!node.selected && node.isCustomDetail) {
          node.isCustomDetail = false;
          this.gridApi.updateRowData({
            remove: [nodes[index + 1].data]
          });
          break;
        }
      }
    }
    
    onRemoveCustomRow(params: any) {
      let customRowIndex = params.rowIndex + 1;
      let removeNode = this.gridApi.getDisplayedRowAtIndex(customRowIndex);
      if(removeNode){
        this.gridApi.updateRowData({
          remove: [removeNode.data]
        })
      };
    }
    
    onSearchBoxChanged(): void {
      let nodes = this.gridApi.getRenderedNodes();
      for (let index = 0; index < nodes.length; index++) {
        let node = nodes[index];
        if(node.isCustomDetail) {
          node.isCustomDetail = false;
          this.gridApi.updateRowData({
            remove: [nodes[index + 1].data]
          });
          break;
        }
      }
      this.gridApi.setQuickFilter(this.searchValue);
      if (this.gridApi.getModel().rootNode.allChildrenCount === 0) {
        this.gridApi.showNoRowsOverlay()
      } else {
        this.gridApi.hideOverlay();
      }
    }

    onSortChanged(params): void{
      this.isSort = params.api.getSortModel().length !== 0;
      let nodes = this.gridApi.getRenderedNodes();
      let setStatusDone = false;
      let removeDone = false;
      for (let index = 0; index < nodes.length; index++) {
        let node = nodes[index];
        if(node.isCustomDetail) {
          node.isCustomDetail = false;
          setStatusDone = true;
        }
        if(node.data.isFullWidthRow) {
          this.gridApi.updateRowData({
            remove: [nodes[index].data]
          });
          removeDone = true;
        }
        if(setStatusDone && removeDone) {
          break;
        }
      }
    }

    hideDetail(): void {
      this.isShowDetail = false;
    }

    ngOnInit() {
      this.defaultConfig = {
        id: 'ag-grid-table-id',
        hideSearch: false,
        showDetailRight: false,
        pagination: true,
        headerHeight: 36,
        rowHeight: 32,
        animateRows: true,
        rowSelection: 'multiple',
        leftMenu: [],
        rightMenu: []
      };
      this.config = Object.assign(this.defaultConfig, this.config);
      if(this.config.columns) {
        this.config.columns.unshift({
          headerName: '',
          maxWidth: 40,
          minWidth: 40,
          headerCheckboxSelection: true,
          headerCheckboxSelectionFilteredOnly: false,        
          checkboxSelection: true,
        });
      }
      this.columnDefs = this.config.columns;
    }

    ngOnChanges(changes: SimpleChanges): void {
      if (this.gridApi) {
        this.rowData = changes.data.currentValue;
        if (this.rowData.length === 0) {
          this.gridApi.showNoRowsOverlay()
        } else {
          this.gridApi.hideOverlay();
        }
        if(this.isShowDetail) {
          this.hideDetail();
          setTimeout(() => {
            this.gridApi.sizeColumnsToFit();
          }, 100);
        } else {
          this.gridApi.sizeColumnsToFit();
        }
      }
    }
  }
