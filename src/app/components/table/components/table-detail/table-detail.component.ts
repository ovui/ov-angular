import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'table-detail',
  templateUrl: './table-detail.component.html',
  styleUrls: ['./table-detail.component.scss']
})
export class TableDetailComponent implements OnInit {

  constructor() { }
  @Input() columns: any = [];
  @Input() selectedItem: any = {};

  ngOnInit() { }

}
