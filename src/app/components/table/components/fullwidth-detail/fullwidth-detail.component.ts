import { Component } from '@angular/core';
import { IFilterAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'fullwidth-detail',
  templateUrl: './fullwidth-detail.component.html',
  styleUrls: ['./fullwidth-detail.component.scss']
})
export class FullwidthDetailComponent implements IFilterAngularComp {
  
  isFilterActive(): boolean {
    return false;
  }
  doesFilterPass(params: import("ag-grid-community").IDoesFilterPassParams): boolean {
    return false;
  }
  getModel() {}
  setModel(model: any): void {}

  columns: any;
  selectedList: any;
  
  agInit(params: any): void {
    this.selectedList = params.api.getSelectedRows();
    this.columns = params.frameworkComponentWrapper.viewContainerRef._view.component.config.columns;
  }
}
