import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { 
  faUser, faHome, faDownload, faPrint, faPlus, faEdit, faTrash, faSync
} from '@fortawesome/free-solid-svg-icons';


// Import share component
import {
    SkeletonComponent, 
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    TableComponent,
    TableDetailComponent,
    FullwidthDetailComponent,
    BreadcrumbComponent,
    AlertComponent,
    DialogComponent
  } from './index.component';

  // Import share service
import { 
  AlertService,
  DialogService
} from './index.service';


const COMPONENTS = [
    SkeletonComponent,
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    TableComponent,
    TableDetailComponent,
    FullwidthDetailComponent,
    BreadcrumbComponent,
    AlertComponent,
    DialogComponent
]

const SERVICES = [
  AlertService,
  DialogService
]

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    RouterModule,
    AgGridModule.withComponents([FullwidthDetailComponent]),
    FontAwesomeModule

  ],
  exports: [
    ...COMPONENTS
  ]
})
export class ComponentModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faUser, faHome, faDownload, faPrint, faPlus, faEdit, faTrash, faSync);
  }
  static forRoot() {
    return {
      ngModule: ComponentModule,
      providers: [
        ...SERVICES
      ]
    }
  }
}
