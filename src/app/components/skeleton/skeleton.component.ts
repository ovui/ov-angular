import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss']
})
export class SkeletonComponent implements OnInit {
  @Input() config: any = {};

  defaultConfig: any = {};
  menuItems: any = [];
  breadcrumbMenu: any = [];

  constructor(private router: Router) { }

  ngOnInit() {
    this.defaultConfig = {
      appId: 'app-id',
      appTitle: 'App name',
      isShowSidebar: true,
      isSidebarHidden: false,
      rightMenu: [],
      menuItems: []
    };
    this.config = Object.assign(this.defaultConfig, this.config);
    this.createBreadcrumbMenu();
  }
  
  createBreadcrumbMenu() {
    let routerList = this.router.url.slice(1).split('/');
    routerList.forEach((route, index) => {
      let item = this.findMenuByRouteId(this.config.menuItems, route);
      if (item) {
        this.breadcrumbMenu.push({
          id: item.id,
          title: item.title
        });
      }
    });
  }
  findMenuByRouteId(menuArray: any, routeId: any) {
    for (let index = 0; index < menuArray.length; index++) {
      const menu = menuArray[index];
      if(menu.id === routeId) {
        return menu;
      } else {
        if(menu.children) {
          const menuFound = this.findMenuByRouteId(menu.children, routeId);
          if(menuFound) {
            return menuFound;
          }
        }
      }
    }
  }
}
