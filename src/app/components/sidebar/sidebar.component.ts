import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'sidebar-component',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() idApp: string = '';
  @Input() menuItems: any = [];

  currentMenu: any = {};

  constructor(private router: Router) { }

  onClickMenuItem(item: any, params: any) {
    if(!item.disabled) {
      this.router.navigate([item.id]);
    }
  }

  ngOnInit() {
    if(this.menuItems.length === 0) {
      console.log('Missing menuItems !');
      return;
    }
  }

}
