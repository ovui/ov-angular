import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService) { }
  currentUser: any = {};

  onLogout() {
    this.authService.logout();
  }

  ngOnInit() {
    let currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUser.name = currentUser.name;
  }

}
