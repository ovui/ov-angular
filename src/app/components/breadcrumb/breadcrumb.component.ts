import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'breadcrumb-component',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  @Input() idApp: string = '';
  @Input() menuItems: any = [];

  currentMenu: any = {};

  constructor(private router: Router) { }

  onHomeClick() {
    this.router.navigate(['dashboard']);
  }
  
  onClickMenuItem(item: any, params: any) {
    if(!item.disabled) {
      this.router.navigate([item.id]);
    }
  }
  
  ngOnInit() {
    if(this.menuItems.length === 0) {
      console.log('Missing menuItems !');
      return;
    }
    this.currentMenu = this.menuItems[this.menuItems.length - 1];
  }

}
