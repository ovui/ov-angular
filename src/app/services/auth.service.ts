import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  public login(credentials: Object){
    return this.http.get('https://5e083e37434a370014168922.mockapi.io/api/login');
  }

  public getCurrentUser(){
    return this.http.get('https://5e083e37434a370014168922.mockapi.io/api/current-user');
  }

  public isLoggedIn(){
    return !!localStorage.getItem('token');

  }

  public logout(){
    localStorage.removeItem('token');
    this.router.navigate(['home/login']);
  }
}
