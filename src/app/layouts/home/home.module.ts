import {NgModule} from '@angular/core'
import {FormsModule} from '@angular/forms'
import {HomeComponent} from './home.component';
import {LoginComponent} from './components'
import { HomeRoutingModule } from './home.routing';

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent
  ],
  imports: [
    HomeRoutingModule,
    FormsModule
  ],
  providers: []
})

export class HomeModule {}
