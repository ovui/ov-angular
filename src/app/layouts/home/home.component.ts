import { Component } from "@angular/core";

@Component({
    selector: 'ov-home',
    template: '<router-outlet></router-outlet>'
})

export class HomeComponent {}
