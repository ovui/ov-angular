import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) {}
  isLoading: Boolean = false;
  loginForm: any = {};

  onLogin() {
    this.isLoading = true;
    this.authService.login(this.loginForm)
      .subscribe((rawRes: any) => {
        // Fake token
        localStorage.setItem('token', rawRes[0].token);
        this.authService.getCurrentUser()
          .subscribe((rawRes: any) => {
            localStorage.setItem('currentUser', JSON.stringify({'name': rawRes.name}));
            this.router.navigate(['/dashboard']);
          }, (errorRes: any) => {
            console.log(errorRes);
            this.isLoading = false
          })
      }, (errorRes) => console.log(errorRes), () => this.isLoading = false)
  }

  ngOnInit() {
  }

}
