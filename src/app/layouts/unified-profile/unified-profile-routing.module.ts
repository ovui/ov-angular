import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnifiedProfileComponent } from './unified-profile.component';
import { CustomerDomainComponent } from './customer-domain/customer-domain.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'customer-domain',
    component: UnifiedProfileComponent
  },
  {
    path: 'customer-domain',
    component: CustomerDomainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnifiedProfileRoutingModule { }
