import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CustomerDomainService {
  cache: any = {
    currentView: 'main',
    currentItem: {}
  };
  
  constructor(private http: HttpClient) { }

  getCustomerDomain() {
    return this.http.get('https://5e083e37434a370014168922.mockapi.io/api/customerDomain');
  }
  createCustomerDomain(requestBody: any) {
    return this.http.post('https://5e083e37434a370014168922.mockapi.io/api/customerDomain', requestBody);
  }
  updateCustomerDomain(requestBody: any) {
    return this.http.put('https://5e083e37434a370014168922.mockapi.io/api/customerDomain/' + requestBody.id, requestBody);
  } 
  deleteCustomerDomain(requestBody: any) {
    return this.http.delete('https://5e083e37434a370014168922.mockapi.io/api/customerDomain/' + requestBody.id, requestBody);
  }
}
