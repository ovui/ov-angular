import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CustomerDomainService } from '../customer-domain.service';
import { AlertService, DialogService } from 'src/app/components/index.service';

const ALERT_ID = 'customer-domain-alert';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @Output() onChangeSelectedList: EventEmitter<any> = new EventEmitter();
  dataView: any = {};
  selectedList: any = [];

  constructor(
    private customerDomainService: CustomerDomainService, 
    private alertService: AlertService,
    private dialogService: DialogService
  ) { }

  getData() {
    this.customerDomainService.getCustomerDomain()
      .subscribe((rawRes: any) => {
        this.dataView.data = rawRes;
      }, (errorRes) => this.alertService.error('Get data fail!', ALERT_ID));
  }
  onSelectedListChanged(data: any) {
    this.selectedList = data;
    this.onChangeSelectedList.emit(this.selectedList);
  }

  ngOnInit() {
    this.dataView.data = [];
    this.dataView.config = {
      id: 'customer-domain-table',
      title: 'Customer Domain List',
      columns: [
        { headerName: 'Customer Domain ID ', field: 'customerDomainId', minWidth: 100, sortable: true },
        { headerName: 'Customer Domain Description ', field: 'customerDomainDescription', minWidth: 100, sortable: true }
      ],
      leftMenu: [],
      rightMenu: [
        {
          id: 'export-csv-btn',
          onClick: function(){
            console.log('Export to .csv');
          },
          title: 'Export to .csv',
          icon: 'download'
        },
        {
          id: 'add-to-report-btn',
          onClick: function(){
            console.log('Add to Report');
          },
          title: 'Add to Report',
        },
        {
          id: 'print-btn',
          onClick: function(){
            console.log('Print');
          },
          title: 'Print',
          icon: 'print'
        }
      ]
    };
    this.getData();
  }

}
