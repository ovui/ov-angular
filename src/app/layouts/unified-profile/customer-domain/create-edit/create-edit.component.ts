import { Component, OnInit, Input } from '@angular/core';
import { CustomerDomainService } from '../customer-domain.service';
import { AlertService } from 'src/app/components/index.service';

const ALERT_ID = 'customer-domain-alert'
@Component({
  selector: 'app-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss']
})
export class CreateEditComponent implements OnInit {
  @Input() cache: any = {};
  constructor(private customerDomainService: CustomerDomainService, private alertService: AlertService) { }

  onCreateForm() {
    this.customerDomainService.createCustomerDomain(this.cache.currentItem)
      .subscribe((rawRes: any)=> {
        this.alertService.success('Create customer domain successfully', ALERT_ID)
        this.cache.currentView = 'main';
      }, (errorRes: any) => {
        this.alertService.error('Create customer domain fail', ALERT_ID)
      })
  }
  onEditForm() {
    this.customerDomainService.updateCustomerDomain(this.cache.currentItem)
      .subscribe((rawRes: any)=> {
        this.alertService.success('Update customer domain successfully', ALERT_ID)
        this.cache.currentView = 'main';
      }, (errorRes: any) => {
        this.alertService.error('Update customer domain fail', ALERT_ID)
      })
  }
  onCancelForm() {
    this.cache.currentView = 'main';
  }

  ngOnInit() {

  }

}
