import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerDomainService } from './customer-domain.service';
import { MainComponent } from './main/main.component';
import { DialogService, AlertService } from 'src/app/components/index.service';

const ALERT_ID = 'customer-domain-alert';
@Component({
  selector: 'app-customer-domain',
  templateUrl: './customer-domain.component.html',
  styleUrls: ['./customer-domain.component.scss']
})
export class CustomerDomainComponent implements OnInit {
  @ViewChild('main',  {static: false}) mainComponent: MainComponent;
  constructor(
    private customerDomainService: CustomerDomainService, 
    private dialogService: DialogService,
    private alertService: AlertService
  ) { }

  cache: any = {};
  skeletonConfig: any = {};
  currentItem: any = {};
  selectedList: any = [];

  onChangeSelectedList(data: any) {
    this.selectedList = data;
  }

  deleteCustomerDomain() {
    this.customerDomainService.deleteCustomerDomain(this.selectedList[0])
      .subscribe((rawRes: any) => {
        this.alertService.success('Delete customer domain successfully', ALERT_ID);
        this.mainComponent.getData();
      }, (errorRes) => this.alertService.error('Delete customer domain fail!', ALERT_ID));
  }

  ngOnInit() {
    this.cache = this.customerDomainService.cache;
    this.skeletonConfig = {
      appId: 'customer-domain',
      appTitle: 'Customer Domain',
      menuItems: [
        {
          id: 'access-guardian',
          title: 'Access Guardian',
          children: [
            {
              id: 'customer-domain',
              title: 'Customer Domain'
            }
          ]
        }
      ],
      rightMenu: [
        {
          id: 'add-btn',
          onClick: () => {
            this.cache.currentItem = {};
            this.cache.currentView = 'create';
          },
          title: '',
          icon: 'plus',
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        },
        {
          id: 'edit-btn',
          onClick: () => {
            this.cache.currentItem = this.selectedList[0];
            this.cache.currentView = 'edit'
          },
          title: '',
          icon: 'edit',
          disabled: () => {
            return this.selectedList.length !== 1;
          },
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        },
        {
          id: 'delete-btn',
          onClick: () => {
            this.dialogService.open({
              id: 'customer-domain-dialog',
              title: 'Delete customer domain',
              body: 'Are you sure you want to delete ' + this.selectedList.length + ' customer domain ?',
              listAction: [
                {
                  title: 'OK',
                  onClick: () => {
                    this.deleteCustomerDomain();
                    this.dialogService.close();
                  }
                }
              ]
            });
          },
          title: '',
          icon: 'trash',
          disabled: () => {
            return this.selectedList.length !== 1;
          },
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        },
        {
          id: 'refesh-btn',
          onClick: () => {
            this.mainComponent.getData();
          },
          title: '',
          icon: 'sync',
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        }
      ]
    }
  }

}
