import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UnifiedProfileRoutingModule } from './unified-profile-routing.module';
import { UnifiedProfileComponent } from './unified-profile.component';
import { CustomerDomainComponent } from './customer-domain/customer-domain.component';
import { ComponentModule } from 'src/app/components/component.module';
import { MainComponent } from './customer-domain/main/main.component';
import { CreateEditComponent } from './customer-domain/create-edit/create-edit.component';
import { CustomerDomainService } from './customer-domain/customer-domain.service';

@NgModule({
  declarations: [
    UnifiedProfileComponent, 
    CustomerDomainComponent,
    MainComponent,
    CreateEditComponent
  ],
  imports: [
    CommonModule,
    ComponentModule.forRoot(),
    UnifiedProfileRoutingModule,
    FormsModule
  ],
  providers: [CustomerDomainService]
})
export class UnifiedProfileModule { }
