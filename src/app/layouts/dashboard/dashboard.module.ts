import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DashboardRoutingModule } from './dashboard.routing';
import { DashboardComponent } from './components/dashboard.component';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    DashboardRoutingModule,
    ComponentModule,
    FormsModule,
    RouterModule
  ],
  providers: []
})

export class DashboardModule {}
