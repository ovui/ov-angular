import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss']
})
export class CreateEditComponent implements OnInit {
  @Input() cache: any = {};
  constructor() { }

  onSubmitForm() {
    this.cache.currentView = 'main'
  }
  onCancelForm() {
    this.cache.currentView = 'main'
  }

  ngOnInit() {

  }

}
