import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DemoService } from '../demo.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @Output() onChangeSelectedList: EventEmitter<any> = new EventEmitter();
  dataView: any = {};
  selectedList: any = [];

  constructor(private demoService: DemoService) { }

  getData() {
    this.demoService.getUsers()
      .subscribe((rawRes: any) => {
        this.dataView.data = rawRes;
      }, (errorRes) => console.log(errorRes));
  }
  onSelectedListChanged(data: any) {
    this.selectedList = data;
    this.onChangeSelectedList.emit(this.selectedList);
  }

  ngOnInit() {
    this.dataView.data = [];
    this.dataView.config = {
      id: 'demo-table',
      title: 'Demo',
      columns: [
        { headerName: 'Name', field: 'name', minWidth: 100, sortable: true },
        { 
          headerName: 'Email', 
          field: 'email', 
          minWidth: 100, 
          cellRenderer: function(params: any) {
            return '<span class="tag-success">' + params.value + "</span>";
          },
          sortable: true
        },
        { headerName: 'Job', field: 'job', minWidth: 100, sortable: true },
        { headerName: 'Phone', field: 'phone', minWidth: 100, sortable: true }
      ],
      leftMenu: [],
      rightMenu: [
        {
          id: 'export-csv-btn',
          onClick: function(){
            console.log('Export to .csv');
          },
          title: 'Export to .csv',
          icon: 'download'
        },
        {
          id: 'add-to-report-btn',
          onClick: function(){
            console.log('Add to Report');
          },
          title: 'Add to Report',
        },
        {
          id: 'print-btn',
          onClick: function(){
            console.log('Print');
          },
          title: 'Print',
          icon: 'print'
        }
      ]
    };
    this.getData();
  }

}
