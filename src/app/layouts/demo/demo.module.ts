import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo.routing';
import { DemoComponent } from './demo.component';
import { ComponentModule } from 'src/app/components/component.module';
import { CreateEditComponent } from './create-edit/create-edit.component';
import { MainComponent } from './main/main.component';
import { FormsModule } from '@angular/forms';
import { DemoService } from './demo.service';


@NgModule({
  declarations: [
    DemoComponent,
    CreateEditComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    ComponentModule,
    FormsModule,
    DemoRoutingModule
  ],
  providers: [DemoService]
})
export class DemoModule { }
