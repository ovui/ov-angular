import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DemoService {
  cache: any = {
    currentView: 'main',
    currentItem: {}
  };
  
  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get('https://5e083e37434a370014168922.mockapi.io/api/users');
  }
}
