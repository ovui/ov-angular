import { Component, OnInit, ViewChild } from '@angular/core';
import { DemoService } from './demo.service';
import { MainComponent } from './main/main.component';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  @ViewChild('main',  {static: false}) mainComponent: MainComponent;
  constructor(private demoService: DemoService) { }

  cache: any = {};
  skeletonConfig: any = {};
  currentItem: any = {};
  selectedList: any = [];

  onChangeSelectedList(data: any) {
    this.selectedList = data;
  }

  ngOnInit() {
    this.cache = this.demoService.cache;
    this.skeletonConfig = {
      appId: 'demo',
      appTitle: 'Demo',
      menuItems: [
        {
          id: 'demo',
          title: 'Demo'
        },
        {
          id: 'access-guardian',
          title: 'Unified Profile'
        }
      ],
      rightMenu: [
        {
          id: 'add-btn',
          onClick: () => {
            this.cache.currentItem = {};
            this.cache.currentView = 'create';
          },
          title: '',
          icon: 'plus',
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        },
        {
          id: 'edit-btn',
          onClick: () => {
            this.cache.currentItem = this.selectedList[0];
            this.cache.currentView = 'edit'
          },
          title: '',
          icon: 'edit',
          disabled: () => {
            return this.selectedList.length !== 1;
          },
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        },
        {
          id: 'delete-btn',
          onClick: () => {
            console.log('Delete btn');
          },
          title: '',
          icon: 'trash',
          disabled: () => {
            return this.selectedList.length !== 1;
          },
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        },
        {
          id: 'refesh-btn',
          onClick: () => {
            this.mainComponent.getData();
          },
          title: '',
          icon: 'sync',
          hidden: () => {
            return this.cache.currentView === 'create' || this.cache.currentView === 'edit'
          } 
        }
      ]
    }
  }

}
