import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { PublicGuard } from './guards/public.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home/login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./layouts/home/home.module').then(home => home.HomeModule), canActivate: [PublicGuard]},
  { path: 'dashboard', loadChildren: () => import('./layouts/dashboard/dashboard.module').then(dashboard => dashboard.DashboardModule), canActivate: [AuthGuard]},
  { path: 'demo', loadChildren: () => import('./layouts/demo/demo.module').then(demo => demo.DemoModule), canActivate: [AuthGuard]},
  { path: 'access-guardian', loadChildren: () => import('./layouts/unified-profile/unified-profile.module').then(unifiedProfile => unifiedProfile.UnifiedProfileModule), canActivate: [AuthGuard]},
  { path: '**', redirectTo: 'home/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
